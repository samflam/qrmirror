class TrivialCompressor {
  constructor() {
  }

  compress(text) {
    return Promise.resolve(new Uint8Array(Buffer.from(text, 'utf-8')));
  }

  decompress(data) {
    return Promise.resolve(Buffer.from(data).toString('utf-8'));
  }
}

module.exports = { TrivialCompressor };
